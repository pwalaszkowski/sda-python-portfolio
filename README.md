# Jan Kowalski

## O mnie
Cześć, nazywam się Jan Kowalski, jestem początkującym programistą Python, w tym miejscu chciałbym podzielić się z Tobą moim doświadczeniem oraz projektami, które miałem dotychczas przyjemność wykonać. 
<center>

![profile](img/profile.jpg)

</center>

## Programowanie
Moje pierwsze kroki w kierunku programowania, stawiałem jeszcze w szkole średniej i były to podstawy języka C++. Studia wybrałem w zupełnie innym kierunku technicznym. Na studiach ponownie pojawiło się programowanie oraz elementy informatyki. Po ukończeniu studiów postanowiłem powrócić do programowana, zgłębiając podstawy języka Python, by następnie pod okiem mentorów w Software Development Academy skupić się na rozwoju w kierunku developera języka Python.

Zawsze lubiłem rozwiązywać problemy oraz ułatwiać sobie powtarzalne czynności, uważam, że programowanie jako narzędzie pozwoli sprawić, że nasze życie stanie się przyjemniejsze.

## Kurs Software Development Academy
Miałem przyjemność uczestniczyć w kursie "Python od Podstaw" organizowanym przez Software Development Academy. Przez ponad 300 godzin zajęć oraz wiele godzin poświęconych na pracę samodzielną zdobyłem wiedzę z następujących tematów:

<center>

![profile](img/python_certificate.png)

</center>

Sprawnie będę poruszał się także w projektach zwinnych, dzięki zajęciom wprowadzających do metodyki Scrum:

<center>

![profile](img/scrum_certificate.png)

</center>

## Git oraz HTTP
W czasie kursu nauczyłem się nie tylko programować, ale rozwijałem swoje umiejętności w wielu kierunkach między innymi:

* Nauczyłem się pracy z Narzędziem GIT (oraz Gitlab)

* Nauczyłem się podstaw REST API oraz narzędzi sieciowych, dzięki czemu w przyszłej pracy, będę mógł stanowić wsparcie w czasie procesu debugowania back-end'u.

## Testowanie Oprogramowania
Wiem, że współczesne projekty powinny się charakteryzować nie tylko dobrze zaprojektowaną, skalowalną oraz łatwo utrzymywalną archiekturą, ale także powinny być 
odpowiednio przetestowane na poziomie testów jednostkowych oraz integracyjnych. Nauczyłem się testować własny kod, dzięki czemu zaoszczędziło mi to sporo czasu 
w czasie rozwijania moich projektów.

## Zadania, które wykonywałem w czasie kursu:
<center>

[Programowanie w Pythonie](Programowanie w Pythonie) | [Praca z Danymi - bazy danych SQL](Praca z Danymi bazy danych SQL) | [Technologie Webowe](Technologie Webowe) |
| [Testowanie Oprogramowania](Testowanie Oprogramowania) | [Algorytmy i Struktury Danych](Algorytmy i Struktury Danych) | [Wzorce Projektowe i Dobre Praktyki](Wzorce Projektowe i Dobre Praktyki)
| [Projekt Praktyczny](Projekt Praktyczny)

</center>

## Moje projekty

* ToDo List

* Twitter SDA

* Simple CHAT

* Simple Forum

* Rent_A_Car

## Technologie

<center>

![profile](img/technical_skills.png)

</center>

## Zainteresowania
Programowanie to nie wszystko, w wolnym czasie eksploruje lasy na rowerze górskim.

<center>

![mtb](img/mtb.jpg)

</center>

## Kontakt

Skontaktuj się ze mną mailowo: developer@google.com

Linkedin: [Jan Kowalski](http://www.linkedin.com)
